package lenz.coffee;

import java.io.*;
import java.net.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import lenz.coffee.interaction.WimpInputHandle;
import lenz.coffee.interaction.AndroidDispatcher;
import lenz.coffee.task.AndroidTask;
import lenz.coffee.task.RemoteWrapperTask;
import lenz.coffee.task.Task;
import lenz.coffee.xbmc.XbmcClient;
import lenz.coffee.xbmc.json.XbmcJsonRpcObject;
import lenz.coffee.xbmc.json.XbmcJsonRpcObjectDeserializer;

public class ServerDriver {
    public static final int SERVER_PORT = 14031;
    public static final String SERVER_IP = "MediaServer";

    public static void main(String argv[]) throws Exception {
        Arbiter.initialize();

        // Kick off xbmc client
        XbmcClient.startXbmcClient();

        ServerSocket welcomeSocket = new ServerSocket(SERVER_PORT);
        while (true) {
            new Connection(welcomeSocket.accept()).startListening();
        }
    }

    private static class Connection implements Runnable {

        Socket mSocketConnection;
        BufferedReader mInput;
        DataOutputStream mOutput;

        private Connection(Socket socketConnection) {
            try {
                mSocketConnection = socketConnection;
                mInput = new BufferedReader(new InputStreamReader(socketConnection.getInputStream()));
                mOutput = new DataOutputStream(socketConnection.getOutputStream());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        public void startListening() {
            new Thread(this).start();
        }

        @Override
        public void run() {
            // Run I/O handling
            try {
                boolean isTouch = false;
                // Establish which interface is connected
                String handle = mInput.readLine();
                if (AndroidDispatcher.isTouchInterface(handle)) {
                    AndroidDispatcher ad = new AndroidDispatcher(mOutput);
                    Arbiter.access().setTouchInterface(ad);
                    Arbiter.access().addReflexListener(ad);
                    isTouch = true;
                } else if (WimpInputHandle.isGestureInterface(handle)) {
                    Arbiter.access().setGestureInterface(new WimpInputHandle(mOutput));
                    System.out.println("Gesture Interface :D");
                } else if (Task.isTaskChannel(handle)) {
                    while (mSocketConnection.isConnected()) {

                        boolean start = Boolean.parseBoolean(mInput.readLine().trim());

                        // Setup parser
                        final Gson gson = new GsonBuilder().setPrettyPrinting()
                                .create();

                        String json = mInput.readLine().trim();
                        System.out.println(start + " " + json);
                        
                        AndroidTask t = gson.fromJson(json, AndroidTask.class);
                        RemoteWrapperTask task = new RemoteWrapperTask(mOutput, t);

                        if (start) {
                            start = Arbiter.access().startTask(task);
                        } else {
                            Arbiter.access().stopTask(task);
                        }

//                        if (!start) {
//                            // This handles the case where the task was not
//                            // started or is being actively stopped.
//                            task.onStop();
//                        }
                    }
                    return;
                } else {
                    throw new IllegalAccessException();
                }

                System.out.println("Connection made");
                while (mSocketConnection.isConnected()) {
                    String s = mInput.readLine();
                    if (s != null) {
                        System.out.println(s);
                    }
                    if (!isTouch) {
                        Arbiter.access().parseMessage(s);
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }

            System.out.println("Connection Ended");
        }
    }
}
