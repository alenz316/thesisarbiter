package lenz.coffee.task;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AndroidTask extends Task {
    private static final String TAG = AndroidTask.class.getSimpleName();
    
    
    protected String packageName;
    
    public AndroidTask(String reflexTask, boolean getAllInput, String packageName) {
        super(reflexTask, getAllInput);
        this.packageName = packageName;
    }
    
    public AndroidTask(){}

    public String getPackageName() {
        return packageName;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AndroidTask)) {
            return false;
        }
        
        return packageName != null ? packageName.equalsIgnoreCase(((AndroidTask)obj).packageName) : false;
    }
    
    public void copyFrom(AndroidTask task) {
        this.consumeAllInput = task.consumeAllInput;
        this.currentlyRunning = task.currentlyRunning;
        this.packageName = task.packageName;
        this.reflexTaskUri = task.reflexTaskUri;
        this.setId(task.getId());
    }
    
    @Override
    public void onStart() {
        super.onStart();
        System.out.println(TAG + ": Started");
    }
    
    @Override
    public void onStop() {
        super.onStop();
        System.out.println(TAG + ": Stopped");
    }
}
