package lenz.coffee.task;

import java.io.DataOutputStream;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RemoteWrapperTask extends Task {

    private DataOutputStream mOutStream;
    private AndroidTask mClientTask;
    
    public RemoteWrapperTask(DataOutputStream outStream, AndroidTask t) {
        super(t.getReflexUri(), t.getAllInput());
        mOutStream = outStream;
        mClientTask = t;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        mClientTask.onStart();
        // Notify client that is started
        dispatchTask();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        mClientTask.onStop();
        // Notify client that it stopped
        dispatchTask();
    }
    
    @Override
    public boolean isCurrentlyRunning() {
        return mClientTask.isCurrentlyRunning();
    }

    
    synchronized private void dispatchTask() {
        mClientTask.currentlyRunning = this.currentlyRunning;
        try {
            final Gson gson = new GsonBuilder().create();
            String s = gson.toJson(mClientTask);

            mOutStream.writeBytes(gson.toJson(mClientTask) + "\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
