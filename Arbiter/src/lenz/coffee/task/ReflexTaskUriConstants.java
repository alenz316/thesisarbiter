package lenz.coffee.task;

public class ReflexTaskUriConstants {

    public static final String REFLEX_MUSIC = "coffee:reflex.music:";
    public static final String REFLEX_INFORMATION_MOVIE = "coffee:reflex.information.movie:";
    
    private ReflexTaskUriConstants(){}
    
}
