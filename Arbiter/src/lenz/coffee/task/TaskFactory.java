package lenz.coffee.task;


public class TaskFactory {

    private static final String GAME_2048 = "by.game.binumbers";
    
    private TaskFactory(){}
    
    public static Task newTask(String packageName) {
        if(GAME_2048.equalsIgnoreCase(packageName)) {
            return new AndroidTask(ReflexTaskUriConstants.REFLEX_MUSIC, true, packageName);
        }
        return null;
    }
    
}
