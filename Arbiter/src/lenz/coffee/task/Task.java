package lenz.coffee.task;

import java.util.List;

import lenz.coffee.interaction.SupervisoryAttentionalSubSystem;

import com.google.common.base.Strings;


public abstract class Task {
    
    public static final String TASK_CHANNEL = "t_c";
    
    private int mId;
//    protected SupervisoryAttentionalSubSystem associtatedSubSystem;
    protected String reflexTaskUri;
    protected boolean consumeAllInput;
    protected boolean currentlyRunning;

    public Task(String reflexUri, boolean getAllInput) {
//        associtatedSubSystem = subSystem;
        reflexTaskUri = reflexUri;
        consumeAllInput = getAllInput;
        mId = -1;
    }
    
    public Task(){}
    
    
	public void onStart() {
	    currentlyRunning = true;
	}
	
	public void onStop() {
	    currentlyRunning = false;
	}
	
	public int getId() {
		return mId;
	}
	public void setId(int id) {
		this.mId = id;
	}
	
	public String getReflexUri() {
	    return reflexTaskUri;
	}
	public boolean getAllInput() {
	    return consumeAllInput;
	}
	
	public boolean isCurrentlyRunning() {
	    return currentlyRunning;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Task)) {
			return false;
		}
		return ((Task)obj).mId == mId;
	}
	
//	public String toString
//	
//	abstract public fromString
	
	public static final int findIndex(List<Task> tasks, int id) {
		for(int i = 0; i < tasks.size(); i++) {
			if (tasks.get(i).getId() == id) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static boolean isTaskChannel(String msg) {
	    return Strings.isNullOrEmpty(msg) ? false: msg.equalsIgnoreCase(TASK_CHANNEL);
	}
	
	
	public final void start() {
	    new Thread(new Runnable() {
            @Override
            public void run() {
                onStart();
            }
        }).start();
	}
}
