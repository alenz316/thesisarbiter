package lenz.coffee;

import java.util.List;

import lenz.coffee.interaction.AndroidDispatcher;
import lenz.coffee.interaction.InputHandle;
import lenz.coffee.interaction.ReflexListener;
import lenz.coffee.interaction.WimpInputHandle;
import lenz.coffee.task.RemoteWrapperTask;
import lenz.coffee.task.Task;
import lenz.coffee.xbmc.XbmcPictureTask;
import TUIO.TuioClient;
import TUIO.TuioCursor;
import TUIO.TuioListener;
import TUIO.TuioObject;
import TUIO.TuioTime;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class Arbiter {
    static Arbiter sArby = null;
    static TouchListener sListener = null;

    WimpInputHandle mGestureInterface;
    AndroidDispatcher mTouchInterface;
    List<ReflexListener> mReflexListeners;

    boolean mLookingUp = true;
    // List<Task> mTasks;
    Task mCurrentTask;
    int mIdCounter = 0;

    private Arbiter() {
        // Kick off listner
        sListener = new TouchListener();

        mReflexListeners = Lists.newArrayList();
        // mTasks = Lists.newArrayList();
    }

    public static void initialize() {
        if (sArby == null) {
            sArby = new Arbiter();
        }
    }

    public static Arbiter access() {
        return sArby;
    }

    public void addReflexListener(ReflexListener list) {
        if (list != null) {
            mReflexListeners.add(list);
        }
    }

    /**
     * Suggest a task to run
     * 
     * @param task
     *            The {@link Task} to run.
     * @return <code>True</code> if allowed to run, <code>false</code> otherwise
     */
    public boolean startTask(final Task task) {
        if (task == null) {
            return false;
        }

        // For now new is always better
        stopTask(mCurrentTask);
        mCurrentTask = task;

        // int id;
        // if(mTasks.contains(task)) {
        // id = task.getId();
        // } else {
        // mTasks.add(task);
        // id = mIdCounter++;
        // }
        // task.setId(id);
        task.setId(mIdCounter++);

        task.start();
        startReflexes(task);

        return true;
    }

    // public void stopTask(int id) {
    // int index = Task.findIndex(mTasks, id);
    // if(index >= 0) {
    // mTasks.remove(index).onStop();
    // }
    // }

    public void stopTask(Task task) {
        // stopTask(mTasks.indexOf(task));
        if (task != null && task.isCurrentlyRunning()) {
            stopReflexes(task);
            task.onStop();
            if(mCurrentTask instanceof RemoteWrapperTask && task instanceof RemoteWrapperTask) {
                mCurrentTask = null;
            }
        }
    }

    protected final void startReflexes(final Task task) {
        for (final ReflexListener listener : mReflexListeners) {
            if (listener != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        listener.handleReflex(task.getReflexUri(), false);
                    }
                }).start();
            }
        }
    }

    protected final void stopReflexes(final Task task) {
        if (task instanceof XbmcPictureTask) {
            // Picture bug :)
            return;
        }

        for (final ReflexListener listener : mReflexListeners) {
            if (listener != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        listener.handleReflex(task.getReflexUri(), true);
                    }
                }).start();
            }
        }
    }

    public void setGestureInterface(WimpInputHandle gestureInterface) {
        this.mGestureInterface = gestureInterface;
    }

    public void setTouchInterface(AndroidDispatcher touchInterface) {
        this.mTouchInterface = touchInterface;
    }

    public void parseMessage(String message) {
        if (Strings.isNullOrEmpty(message)) {
            // Do nothing
            return;
        }

        if (InputHandle.isGesture(message)) {
            handleGestureInput(message);
        }
    }

    private static float X_UPPER = 0.91f;
    private static float X_LOWER = 0.18f;
    private static float X_SCALE = 1 / (X_UPPER - X_LOWER);
    private static float Y_UPPER = 0.8f;
    private static float Y_LOWER = 0.3f;
    private static float Y_SCALE = 1 / (Y_UPPER - Y_LOWER);

    private void handleTouch(int id, int action, float x, float y, float speedX, float speedY) {
        if (id > InputHandle.SECOND_FINGER_ID) {
            return;
        }

        // Normalize points based on offset
        // Subtract lower bounds
        x = x - X_LOWER;
        y = y - Y_LOWER;
        // Multiply scaling factor
        x *= X_SCALE;
        y *= Y_SCALE;

        // Now invert :) MATHS
        x = 1 - x;
        y = 1 - y;

//        System.out.println("x: " + x + " y: " + y);
        if ((x > 1 || x < 0) || (y > 1 || y < 0)) {
//            System.out.println("OUT OF RANGE x: " + x + " y: " + y);
            return;
        }

        if (mGestureInterface != null) {// && mLookingUp) {
            mGestureInterface.consumeTouch(id, action, x, y, speedX, speedY);
        }
        if (mTouchInterface != null) {
            mTouchInterface.consumeTouch(id, action, x, y, speedX, speedY);
        }
    }

    private void handleGestureInput(String input) {
        if (InputHandle.hasAttentionChanged(input)) {
            mLookingUp = InputHandle.isLookingUp(input);
            return;
        }
        // Hack, remote tasks consume all gestures
        if (mGestureInterface != null && !(mCurrentTask instanceof RemoteWrapperTask)) {// && mLookingUp) {
            mGestureInterface.consumeGesture(input);
        } else if (mTouchInterface != null) {
            if(InputHandle.isClick(input)) {
                mGestureInterface.consumeGesture(input);
            } else {
                mTouchInterface.consumeGesture(input);
            }
        }
    }

    private class TouchListener implements TuioListener, Runnable {

        TuioClient mTouchClient;

        private TouchListener() {
            mTouchClient = new TuioClient();
            mTouchClient.addTuioListener(this);
            new Thread(this).start();
        }

        @Override
        public void run() {
            // Run connection on separate thread
            mTouchClient.connect();
        }

        @Override
        public void addTuioCursor(TuioCursor c) {
//            System.out.println("Add: C" + c.getCursorID());
            handleTouch(c.getCursorID(), InputHandle.ACTION_DOWN, c.getX(), c.getY(), c.getXSpeed(), c.getYSpeed());
        }

        @Override
        public void updateTuioCursor(TuioCursor c) {
            // System.out.println("Update: C" + );
            handleTouch(c.getCursorID(), InputHandle.ACTION_MOVE, c.getX(), c.getY(), c.getXSpeed(), c.getYSpeed());
        }

        @Override
        public void removeTuioCursor(TuioCursor c) {
//            System.out.println("Remove: C" + c.getCursorID());
            handleTouch(c.getCursorID(), InputHandle.ACTION_UP, c.getX(), c.getY(), c.getXSpeed(), c.getYSpeed());
        }

        @Override
        public void updateTuioObject(TuioObject arg0) {
            System.out.println("Update: " + arg0.getSymbolID());
        }

        @Override
        public void removeTuioObject(TuioObject arg0) {
            System.out.println("Remove: " + arg0.getSymbolID());
        }

        @Override
        public void addTuioObject(TuioObject arg0) {
            System.out.println("Add: " + arg0.getSymbolID());
        }

        @Override
        public void refresh(TuioTime arg0) {
            // System.out.println("Refresh");
        }

    }
}
