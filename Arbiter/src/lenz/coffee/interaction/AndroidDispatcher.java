package lenz.coffee.interaction;

import java.io.DataOutputStream;
import java.io.IOException;

public class AndroidDispatcher extends InputHandle implements ReflexListener {

    public static final String TOUCH_HANDLE = "t_h";

    public static boolean isTouchInterface(String handle) {
        return TOUCH_HANDLE.equalsIgnoreCase(handle);
    }
    
    public static final boolean isReflex(String reflex) {
        return hasPrefix(ReflexListener.REFLEX_COMMAND, reflex);
    }

    public AndroidDispatcher(DataOutputStream output) {
        super(output);
    }

    public void consumeTouch(int index, int action, float x, float y, float speedX, float speedY) {
            String message = index + " " + action + " " + x + " " + y;
            dispatch(message);
    }

    @Override
    public void consumeGesture(String gesture) {
        dispatch(gesture);
    }

    @Override
    public void handleReflex(String reflexTaskUri, boolean kill) {
        dispatch(REFLEX_COMMAND+(kill?1:0)+reflexTaskUri);
    }

    synchronized private void dispatch(String message) {
        try {
            mNetworkOutput.writeBytes(message + "\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
