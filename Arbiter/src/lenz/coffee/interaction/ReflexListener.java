package lenz.coffee.interaction;

public interface ReflexListener {
    public static final String REFLEX_COMMAND = "r_";
    
    public void handleReflex(String reflexTaskUri, boolean kill);
}
