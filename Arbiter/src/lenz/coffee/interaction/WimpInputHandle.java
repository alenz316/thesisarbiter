package lenz.coffee.interaction;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.DataOutputStream;

import lenz.coffee.xbmc.XbmcDelegate;

public class WimpInputHandle extends InputHandle {

	public static final String GESTURE_HANDLE = "g_h";

	private Robot mRobot;
	private int mWidth;
	private int mHeight;
	private int mCurrentMouseId;
	private long mSecondFingerStartTime;

	public static boolean isGestureInterface(String handle) {
		return GESTURE_HANDLE.equalsIgnoreCase(handle);
	}

	public WimpInputHandle(DataOutputStream output) {
		super(output);
		try {
			mRobot = new Robot();
		} catch (AWTException ex) {
			ex.printStackTrace();
		}
		
		mWidth  = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		mHeight = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		mCurrentMouseId = -1;
		mSecondFingerStartTime = -1;
	}
 
	@Override
	public void consumeTouch(int id, int action, float x, float y, float speedX, float speedY) {
		switch(action) {
			case ACTION_DOWN:
				onRelativeDown(id, action, x, y, speedX, speedY);
				break;
			case ACTION_MOVE:
				onRelativeMove(id, action, x, y, speedX, speedY);
				break;
			case ACTION_UP:
				onRelativeUp(id, action, x, y, speedX, speedY);
				break;
		}
	}
	
	private void onRelativeDown(int id, int action, float x, float y, float speedX, float speedY) {
		Point pos = MouseInfo.getPointerInfo().getLocation();
		int xpos = pos.x-(int)Math.round(speedX*Math.sqrt(mWidth));
		if(xpos<0) xpos=0; if(xpos>mWidth) xpos=mWidth;
		int ypos = pos.y-(int)Math.round(speedY*Math.sqrt(mHeight));
		if(ypos<0) ypos=0; if(ypos>mHeight) ypos=mHeight;
		
		if (mCurrentMouseId < 0) {
			mCurrentMouseId = id;
			if (mRobot!=null) mRobot.mouseMove(xpos,ypos);
		} else {
			if(id == SECOND_FINGER_ID) {
				// Second Finger
				mSecondFingerStartTime = System.currentTimeMillis();
			}
//			if (mRobot!=null) mRobot.mousePress(InputEvent.BUTTON1_MASK);
		}
	}
	
	private void onRelativeMove(int id, int action, float x, float y, float speedX, float speedY) {
		Point pos = MouseInfo.getPointerInfo().getLocation();
		int xpos = pos.x-(int)Math.round(speedX*Math.sqrt(mWidth));
		if(xpos<0) xpos=0; if(xpos>mWidth) xpos=mWidth;
		int ypos = pos.y-(int)Math.round(speedY*Math.sqrt(mHeight));
		if(ypos<0) ypos=0; if(ypos>mHeight) ypos=mHeight;
		if (mCurrentMouseId==id) {
			if (mRobot!=null) mRobot.mouseMove(xpos,ypos);
		} else if (id == SECOND_FINGER_ID) {
			if (isPastTouchTapTimeout()) {
				float yPosDiff = pos.y-ypos;
				if (Math.abs(yPosDiff) > SCROLL_THRESHOLD) {
					mRobot.mouseWheel(-((int) Math.signum(yPosDiff)));
				}
			}
		}
	}
	
	private void onRelativeUp(int id, int action, float x, float y, float speedX, float speedY) {
		if (mCurrentMouseId==id) {
			mCurrentMouseId=-1;
		} else {
			if (id != SECOND_FINGER_ID || !isPastTouchTapTimeout())  {
				mRobot.mousePress(InputEvent.BUTTON1_MASK);
				mRobot.mouseRelease(InputEvent.BUTTON1_MASK);
			}
		}
	}
	
	private boolean isPastTouchTapTimeout() {
		return (System.currentTimeMillis() - mSecondFingerStartTime) > TAP_WAIT_DURATION;
	}

	
	public static long MIN_CLICK_TIME = 100;
	public static long MAX_CLICK_TIME = 1000;
	private long mlastTime = System.currentTimeMillis();
	
	@Override
	public void consumeGesture(String gesture) {
	    String splitString[] = gesture.split(";");
	    
	    if(InputHandle.isLeftSwipe(gesture)) {
	        XbmcDelegate.goLeft();
	    } else if(InputHandle.isRightSwipe(gesture)) {
	        XbmcDelegate.goRight();
	    } else if(InputHandle.isRightFling(gesture)) {
            XbmcDelegate.enter();
        } else if(InputHandle.isLeftFling(gesture)) {
            XbmcDelegate.exit();
        } else if(InputHandle.isGrabAndHold(splitString[0])) {
	        onAbsoluteMove(Float.parseFloat(splitString[1]), Float.parseFloat(splitString[2]));
	        /*long curTime = System.currentTimeMillis();
	        long difTime = curTime-mlastTime;
	        
	        if(difTime > MIN_CLICK_TIME && difTime < MAX_CLICK_TIME) {
	            mRobot.mousePress(InputEvent.BUTTON1_MASK);
	            mRobot.mouseRelease(InputEvent.BUTTON1_MASK);
	        }
	        mlastTime = curTime;*/
	    }
	    else if(InputHandle.isClick(gesture))
	    {
	        XbmcDelegate.next();
//            mRobot.mouseRelease(InputEvent.BUTTON1_MASK);
//            mRobot.mousePress(InputEvent.BUTTON1_MASK);
	    }

	}

	
	private static final float MAX_WIDTH = 0.35f;
	private static final float MAX_HEIGHT = 0.2f;
    public void onAbsoluteMove(float x, float y) {
        float maxWidth = MAX_WIDTH * 2;
        float maxHeight = MAX_HEIGHT * 2;
        
        x += MAX_WIDTH;
        y += MAX_HEIGHT;
        
        if (x > maxWidth) {
            x = maxWidth;
        } else if (x < 0) {
            x = 0;
        }
        
        System.out.println("x:" + x + " y: " + y );
        
        if (y > maxHeight) {
            y = maxHeight;
        } else if (y < 0) { 
            y = 0;
        }
        
        x = (x/maxWidth)*mWidth;
        y = mHeight - ((y/maxHeight)*mHeight);
        
        if (mRobot!=null) mRobot.mouseMove((int) x, (int) y);
    }
}
