package lenz.coffee.interaction;

import java.io.DataOutputStream;

import com.google.common.base.Strings;

public abstract class InputHandle {

	// From Android MotionEventClass
	public static final int ACTION_DOWN = 0;
	public static final int ACTION_UP = 1;
	public static final int ACTION_MOVE = 2;
	public static final int ACTION_CANCEL = 3;

	public static final int SECOND_FINGER_ID = 1;
	public static final long TAP_WAIT_DURATION = 150;
	public static final long SCROLL_THRESHOLD = 25;
	
	
	public static final String GESTURE_COMMAND = "g_";
	public static final String ATTENTION_ENGAGED = "Engaged";
	public static final String ATTENTION_DISENGAGED = "Disengaged";
	
	public static final String GESTURE_GRABHOLD = "GrabAndHold";
	public static final String GESTURE_RIGHT_SWIPE = "RightHandSwipe";
	public static final String GESTURE_LEFT_SWIPE = "LeftHandSwipe";
	public static final String GESTURE_RIGHT_FLING = "RightFling";
	public static final String GESTURE_LEFT_FLING = "LeftFling";
	public static final String GESTURE_PINCH = "Pinch";
	public static final String GESTURE_LOOKING_UP = "LookingUp";
	public static final String GESTURE_PREFIX_LOOKING = "Looking";

	public static final String POSITION_BEHIND = "BehindTable";
	public static final String POSITION_LEFT = "LeftOfTable";
	public static final String POSITION_RIGHT = "RightOfTable";
	public static final String POSITION_FRONT = "FrontOfTable";

	DataOutputStream mNetworkOutput;

	public InputHandle(DataOutputStream output) {
		mNetworkOutput = output;
	}

	public final void consumeTouch(int index, int action, float x, float y) {
		consumeTouch(index, action, x, y, 0f, 0f);
	}

	abstract public void consumeTouch(int index, int action, float x, float y,
			float speedX, float speedY);

	abstract public void consumeGesture(String gesture);
	
	public static final boolean hasAttentionChanged(String gesture) {
	    return gesture.contains(InputHandle.GESTURE_PREFIX_LOOKING);
	}
	
	public static final boolean isLookingUp(String gesture) {
        return gesture.contains(InputHandle.GESTURE_LOOKING_UP);
    }
	
	public static final boolean isGesture(String gesture) {
		return hasPrefix(GESTURE_COMMAND, gesture);
	}
	
	public static final boolean isLeftSwipe(String gesture) {
	    return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_LEFT_SWIPE);
	}
	
	public static final boolean isRightSwipe(String gesture) {
        return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_RIGHT_SWIPE);
    }

	public static final boolean isRightFling(String gesture) {
        return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_RIGHT_FLING);
    }
	
	public static final boolean isLeftFling(String gesture) {
        return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_LEFT_FLING);
    }
	
	public static final boolean isGrabAndHold(String gesture) {
        return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_GRABHOLD);
    }
	
	public static final boolean isClick(String gesture) {
        return gesture.equalsIgnoreCase(GESTURE_COMMAND + InputHandle.GESTURE_PINCH);
    }
	
	protected static final boolean hasPrefix(String prefix, String s) {
	    return Strings.isNullOrEmpty(s) ? false : s.startsWith(prefix);
	}
}
