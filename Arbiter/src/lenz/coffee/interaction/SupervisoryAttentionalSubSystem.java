package lenz.coffee.interaction;

import lenz.coffee.task.Task;

public interface SupervisoryAttentionalSubSystem {
    
    public void proposeTask(Task task);
    
    public Task getCurrentTask();

}
