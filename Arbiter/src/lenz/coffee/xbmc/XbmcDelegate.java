package lenz.coffee.xbmc;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import lenz.coffee.xbmc.json.OmdbInformation;
import lenz.coffee.xbmc.json.XbmcJsonRpcObject;
import lenz.coffee.xbmc.json.XbmcJsonRpcObjectDeserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class XbmcDelegate implements Runnable {

    private static Thread sThread;
    private static OnPlaybackListener sListener;
    private static OmdbInformation sLastPlayedInfo;
    private static String sLastFile;
    private static String sLastKnownTitle;
    private static DataOutputStream sOutputStream;
    private static Robot mRobot;

    private static String sLastPlayerId;

    public interface OnPlaybackListener {
        public void onPlay(boolean isVideo);

        public void onPause();

        public void onStop();
    }

    public static void initialize() {
        if (sThread == null) {

            sThread = new Thread(new XbmcDelegate());
            sThread.start();

            try {
                mRobot = new Robot();
            } catch (AWTException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void setOnPlayListener(OnPlaybackListener listener) {
        sListener = listener;
    }

    public static OmdbInformation getLastPlayedInfo() {
        return sLastPlayedInfo;
    }

    public static String getLastKnownTitle() {
        return sLastKnownTitle;
    }

    public static String getLastFile() {
        return sLastFile;
    }

    public static void goRight() {
        pressKey(KeyEvent.VK_NUMPAD6);
    }

    public static void goLeft() {
        pressKey(KeyEvent.VK_NUMPAD4);
    }

    public static void enter() {
        pressKey(KeyEvent.VK_ENTER);
    }

    public static void exit() {
        pressKey(KeyEvent.VK_ESCAPE);
    }
    
    public static void next() {
        pressKey(KeyEvent.VK_PERIOD);
    }

    public static void callPlayPause() {
        pressKey(KeyEvent.VK_SPACE);
        // dispatch("{\"jsonrpc\": \"2.0\", \"method\": \"Player.PlayPause\", \"params\": { \"playerid\": "+sLastPlayerId+" }, \"id\": 1}");
    }

    public static void callStop() {
        pressKey(KeyEvent.VK_X);
    }

    public static void callPlayFrozen() {
        String playFrozen = "{\"jsonrpc\":\"2.0\",\"id\":\"1\",\"method\":\"Player.Open\",\"params\":{\"item\":{\"file\":\"C:/Users/Anthony/AppData/Roaming/XBMC/userdata/playlists/music/Frozen.m3u\"}}}";
        System.out.println(playFrozen);
        dispatch(playFrozen);
    }

    static private void pressKey(int keycode) {
        try {
            mRobot = new Robot();
            mRobot.keyPress(keycode);
            mRobot.keyRelease(keycode);
        } catch (AWTException ex) {
            ex.printStackTrace();
        }
    }

    private XbmcDelegate() {
    }

    @Override
    public void run() {
        try {
            // Localhost and xbmc default port
            Socket clientSocket = new Socket("127.0.0.1", 9090);

            // Setup communication channels
            sOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inChannel = new Reader(new InputStreamReader(clientSocket.getInputStream()));

            // Setup parser
            final Gson gson = new GsonBuilder().setPrettyPrinting()
                    .registerTypeAdapter(XbmcJsonRpcObject.class, new XbmcJsonRpcObjectDeserializer()).create();
            JsonReader reader = new JsonReader(inChannel);
            reader.setLenient(true);

            // Forever read/write and parse
            while (clientSocket.isConnected()) {
                try {
                    XbmcJsonRpcObject c = gson.fromJson(reader, XbmcJsonRpcObject.class);

                    if (c.isPauseCommand()) {
                        sListener.onPause();
                    } else if (c.isStopCommand()) {
                        sListener.onStop();
                    } else if (c.isPlayCommand()) {
                        if (c.getAddedFile() != null && c.isPicture()) {
                            System.out.println(c.getAddedFile());
                            sLastFile = c.getAddedFile();
                            if (sListener != null) {
                                sListener.onPlay(false);
                            }
                        } else if (c.isVideo()) {
                            // Step 1: Get active player
                            c = XbmcJsonRpcObject.newGetActivPlayerRequest();
                            String request = gson.toJson(c);
                            dispatch(request);
                            getPlayInfo(gson, reader);
                        }
                    }
                } catch (JsonSyntaxException ex) {
                    ex.printStackTrace();
                    // Keep calm and listen on
                } catch (URISyntaxException ex) {
                    ex.printStackTrace();
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void getPlayInfo(Gson gson, JsonReader reader) throws URISyntaxException {
        XbmcJsonRpcObject c = gson.fromJson(reader, XbmcJsonRpcObject.class);

        if (c.isResult()) {

            // Step 2: Get the current playing item
            sLastPlayerId = c.getResult().getPlayerId();
            c = XbmcJsonRpcObject.newGetItem(c.getResult());
            String request = gson.toJson(c);
            dispatch(request);
        }

        c = gson.fromJson(reader, XbmcJsonRpcObject.class);
        if (c.isResult() && XbmcJsonRpcObject.ID_GET_VIDEO_ITEM.equalsIgnoreCase(c.getId())) {
            // Step 3: Get the IMDB url
            sLastKnownTitle = c.getTitle();
            URI uri = new URI("http", "www.omdbapi.com", "/", "t=" + sLastKnownTitle, null);
            sLastPlayedInfo = gson.fromJson(readUrl(uri.toASCIIString()), OmdbInformation.class);
            if (sListener != null) {
                sListener.onPlay(true);
            }
        }
    }

    private static String readUrl(String urlString) {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static class Reader extends BufferedReader {

        public Reader(java.io.Reader in) {
            super(in);
            // TODO Auto-generated constructor stub
        }

        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            int rtrn = super.read(cbuf, off, len);

            System.out.println(new String(cbuf));
            return rtrn;
        }
    }

    synchronized private static void dispatch(String message) {
        try {
            sOutputStream.writeBytes(message);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
