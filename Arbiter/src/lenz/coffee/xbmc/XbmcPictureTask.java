package lenz.coffee.xbmc;

import lenz.coffee.task.Task;

public class XbmcPictureTask extends Task {

	public XbmcPictureTask(String reflexTask, boolean getAllInput) {
        super(reflexTask, getAllInput);
    }

    @Override
	public void onStart() {
        
        super.onStart();
		System.out.println("Task started: " + XbmcDelegate.getLastFile());
	}

	@Override
	public void onStop() {
//	    if(isCurrentlyRunning()) {
//	        XbmcDelegate.callStop();
//	    }
	    
	    super.onStop();
		System.out.println("STOPPED");
	}
	
}
