package lenz.coffee.xbmc;

import java.io.File;
import java.io.IOException;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.GpsDirectory;

import lenz.coffee.Arbiter;
import lenz.coffee.interaction.ReflexListener;
import lenz.coffee.interaction.SupervisoryAttentionalSubSystem;
import lenz.coffee.interaction.WimpInputHandle;
import lenz.coffee.task.ReflexTaskUriConstants;
import lenz.coffee.task.Task;
import lenz.coffee.xbmc.XbmcDelegate.OnPlaybackListener;
import lenz.coffee.xbmc.json.OmdbInformation;

public class XbmcClient implements OnPlaybackListener, SupervisoryAttentionalSubSystem, ReflexListener {

    private static XbmcClient singleton = null;

    private Task mCurrentTask = null;

    public static void startXbmcClient() {
        if (singleton == null) {
            singleton = new XbmcClient();
            XbmcDelegate.initialize();
            XbmcDelegate.setOnPlayListener(singleton);

            Arbiter.access().setGestureInterface(new WimpInputHandle(null));
            Arbiter.access().addReflexListener(singleton);
        }
    }

    private XbmcClient() {
    }

    @Override
    public void onPlay(boolean isVideo) {
        //if (mCurrentTask == null) {// || !mCurrentTask.isCurrentlyRunning()) {
            if (isVideo) {
                proposeTask(new XbmcVideoTask(getReflexVideoTaskUri(), false));
            } else {
                proposeTask(new XbmcPictureTask(getReflexPictureTaskUri(), false));
            }
        //}
    }

    @Override
    public void onPause() {
        stopCurrentTask();
    }

    @Override
    public void onStop() {
        stopCurrentTask();
    }

    private String getReflexVideoTaskUri() {
        OmdbInformation info = XbmcDelegate.getLastPlayedInfo();

        if (info != null) {
            return "imdb:///title/" + info.getImdbID();
        } else {
            return "imdb:///find?q=" + XbmcDelegate.getLastKnownTitle();
        }
    }

    private String getReflexPictureTaskUri() {
        String file = XbmcDelegate.getLastFile();

        File jpegFile = new File(file);
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(jpegFile);

            GpsDirectory dir = metadata.getDirectory(GpsDirectory.class);
            GeoLocation geo = dir.getGeoLocation();
            System.out.println("lat:" + geo.getLatitude() + " long:" + geo.getLongitude());

            if (geo != null) {
                return "geo:" + geo.getLatitude() + "," + geo.getLongitude();
            }
        } catch (ImageProcessingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void stopCurrentTask() {
        if (mCurrentTask != null) {
            Arbiter.access().stopTask(mCurrentTask);
        }
    }

    @Override
    public void proposeTask(Task task) {
        stopCurrentTask();

        mCurrentTask = task;
        // TODO Needs to handle if the task is denied!!!!!!!!!!!
        Arbiter.access().startTask(mCurrentTask);

    }

    @Override
    public Task getCurrentTask() {
        return mCurrentTask;
    }

    @Override
    public void handleReflex(String reflexTaskUri, boolean kill) {
        System.out.println(reflexTaskUri);
        if (ReflexTaskUriConstants.REFLEX_MUSIC.equals(reflexTaskUri)) {
            if (!kill) {
                XbmcDelegate.callPlayFrozen();
            } else {
                XbmcDelegate.callStop();
            }
        }
    }
}
