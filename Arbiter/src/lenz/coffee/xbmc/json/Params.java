package lenz.coffee.xbmc.json;

public class Params {
	public static final String[] ALL_PROPS = { "title", "album", "artist",
			"duration", "thumbnail", "file", "fanart", "streamdetails" };
	protected Data data;
	protected String sender;
	protected String[] properties;
	protected int playerid;
}
