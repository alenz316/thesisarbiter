package lenz.coffee.xbmc.json;

import java.util.List;

public class XbmcJsonRpcObject {

	// For documentation see
	// http://wiki.xbmc.org/index.php?title=JSON-RPC_API/Examples
	public static final String RPC_VERSION = "2.0";
	public static final String METHOD_ON_PLAY = "Player.OnPlay";
	public static final String METHOD_ON_PAUSE = "Player.OnPause";
	public static final String METHOD_ON_STOP = "Player.OnStop";
	public static final String METHOD_GET_ACTIVE_PLAYERS = "Player.GetActivePlayers";
	public static final String METHOD_GET_ITEM = "Player.GetItem";
	public static final String ID_GET_AUDIO_ITEM = "AudioGetItem";
	public static final String ID_GET_VIDEO_ITEM = "VideoGetItem";
	public static final String ID_GET_PICTURE_ITEM = "PictureGetItem";
	public static final String DATA_ITEM_TYPE_PICTURE = "picture";
	public static final String DATA_ITEM_TYPE_VIDEO = "movie";

	protected String jsonrpc;
	protected String id;
	protected String method;
	// Use custom name for 'result' to prevent gson from trying to parse it
	protected List<Result> resultants;
	protected Params params;

	public XbmcJsonRpcObject(String id, String method, List<Result> results,
			Params params) {
		jsonrpc = RPC_VERSION;
		this.id = id;
		this.method = method;
		this.resultants = results;
		this.params = params;
	}

	private XbmcJsonRpcObject() {
		jsonrpc = RPC_VERSION;
	}

	public String getId() {
		return id;
	}

	public boolean isPlayCommand() {
		return METHOD_ON_PLAY.equalsIgnoreCase(method);
	}

	public boolean isPauseCommand() {
		return METHOD_ON_PAUSE.equalsIgnoreCase(method);
	}

	public boolean isStopCommand() {
        return METHOD_ON_STOP.equalsIgnoreCase(method);
    }
	
	public boolean isResult() {
		return (resultants != null && resultants.size() > 0);
	}

	public Result getResult() {
		return isResult() ? resultants.get(0) : null;
	}

	public String getTitle() {
		// If has title
		if (isResult()) {
			Item i = resultants.get(0).item;
			if (i != null) {
				return i.title;
			}
		}
		return null;
	}
	
	public String getAddedFile() {
	    if(params.data.item.file != null) {
	        return params.data.item.file;
	    }
	    return null;
	}
	
	public boolean isPicture() {
	    return DATA_ITEM_TYPE_PICTURE.equalsIgnoreCase(params.data.item.type);
	}
	
	public boolean isVideo() {
        return DATA_ITEM_TYPE_VIDEO.equalsIgnoreCase(params.data.item.type);
    }

	public static XbmcJsonRpcObject newGetActivPlayerRequest() {
		XbmcJsonRpcObject obj = new XbmcJsonRpcObject();

		obj.method = METHOD_GET_ACTIVE_PLAYERS;
		obj.id = "1";
		return obj;
	}

	public static XbmcJsonRpcObject newGetItem(Result res) {
		XbmcJsonRpcObject obj = new XbmcJsonRpcObject();

		obj.method = METHOD_GET_ITEM;
		if (res.isAudioType()) {
			obj.id = ID_GET_AUDIO_ITEM;
		} else if (res.isVideoType()) {
			obj.id = ID_GET_VIDEO_ITEM;
		} else if (res.isPictureType()) {
			obj.id = ID_GET_PICTURE_ITEM;
		}

		Params p = new Params();
		p.properties = Params.ALL_PROPS;
		p.playerid = Integer.parseInt(res.playerid);
		obj.params = p;
		return obj;
	}
}
