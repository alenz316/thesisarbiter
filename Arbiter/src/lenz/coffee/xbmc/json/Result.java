package lenz.coffee.xbmc.json;

public class Result {
	public static final String KEY_RESULT = "result";

	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_PICTURE = "picture";
	protected String playerid;
	protected String type;
	protected Item item;

	public boolean isVideoType() {
		return TYPE_VIDEO.equalsIgnoreCase(type);
	}

	public boolean isAudioType() {
		return TYPE_AUDIO.equalsIgnoreCase(type);
	}

	public boolean isPictureType() {
		return TYPE_PICTURE.equalsIgnoreCase(type);
	}
	
	public String getPlayerId() {
	    return playerid;
	}
}
