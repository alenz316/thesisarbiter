package lenz.coffee.xbmc.json;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class XbmcJsonRpcObjectDeserializer implements
		JsonDeserializer<XbmcJsonRpcObject> {
	// Compliments of
	// http://stackoverflow.com/questions/6014674/gson-custom-deseralizer-for-one-variable-in-an-object
	@Override
	public XbmcJsonRpcObject deserialize(JsonElement element, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		JsonObject jsonObj = element.getAsJsonObject();

		Gson g = new Gson();

		XbmcJsonRpcObject XbmcJsonRpcObject = g.fromJson(element,
				XbmcJsonRpcObject.class);

		if (jsonObj.has(Result.KEY_RESULT)) {
			List<Result> results = null;
			// Check to see if we were given a list or a single result
			if (jsonObj.get(Result.KEY_RESULT).isJsonArray()) {
				// if it's a list, just parse that from the JSON
				results = g.fromJson(jsonObj.get(Result.KEY_RESULT),
						new TypeToken<List<Result>>() {
							private static final long serialVersionUID = 1L;
						}.getType());
			} else {
				// otherwise, parse the single result,
				// and add it to the list
				Result single = g.fromJson(jsonObj.get(Result.KEY_RESULT),
						Result.class);
				results = new ArrayList<Result>();
				results.add(single);
			}
			// set the correct result list
			XbmcJsonRpcObject.resultants = results;
		}

		return XbmcJsonRpcObject;
	}
}
