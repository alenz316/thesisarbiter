package lenz.coffee.touch;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;

import lenz.coffee.ServerDriver;
import lenz.coffee.interaction.SupervisoryAttentionalSubSystem;
import lenz.coffee.task.AndroidTask;
import lenz.coffee.task.Task;
import lenz.coffee.task.TaskFactory;
import android.app.ActivityManager;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AndroidSupervisoryAttentionalSubSystem implements SupervisoryAttentionalSubSystem, Runnable {

    public static final String TAG = AndroidSupervisoryAttentionalSubSystem.class.getSimpleName();

    private static AndroidSupervisoryAttentionalSubSystem singleton = null;

    public static AndroidSupervisoryAttentionalSubSystem instance(Context ctx, Handler handler) {
        if (singleton == null) {
            singleton = new AndroidSupervisoryAttentionalSubSystem(ctx, handler);
        }

        return singleton;
    }

    private Context mContext;
    private Handler mHandler;
    private Socket mSocket;
    private DataOutputStream mOutToServer;
    private BufferedReader mInFromServer;
    private ActivityManager mActivityManager;

    private String mLastPackageName = null;
    private AndroidTask mCurrentTask = null;

    private AndroidSupervisoryAttentionalSubSystem(Context ctx, Handler handler) {
        this.mContext = ctx;
        this.mHandler = handler;
        this.mActivityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

        // Template derived from
        // http://systembash.com/content/a-simple-java-tcp-server-and-tcp-client/
        try {
            this.mSocket = new Socket(ServerDriver.SERVER_IP, ServerDriver.SERVER_PORT);
            this.mOutToServer = new DataOutputStream(mSocket.getOutputStream());
            this.mInFromServer = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

            synchronized (mOutToServer) {
                // Establish connection as a task channel
                mOutToServer.writeBytes(AndroidTask.TASK_CHANNEL + "\n");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        // Return a list of the tasks that are currently running,
        // with the most recent being first and older ones after in order.
        // Taken 1 inside getRunningTasks method means want to take only
        // top activity from stack and forgot the olders.
        synchronized (mActivityManager) {

            List<ActivityManager.RunningTaskInfo> taskInfo = mActivityManager.getRunningTasks(1);

            String currentRunningPackageName = taskInfo.get(0).topActivity.getPackageName();

            if (hasPackageChanged(currentRunningPackageName)) {
                mLastPackageName = currentRunningPackageName;
                // Log.e(TAG, "Last: " + mLastPackageName + " Current: " +
                // currentRunningPackageName);

                AndroidTask t = (AndroidTask) TaskFactory.newTask(currentRunningPackageName);

                if (t != null) {
                    proposeTask(t);
                } else if (mCurrentTask != null) {
                    stopTask(mCurrentTask);
                }

            }
        }
    }

    /**
     * Used to determine if the service has detected a change in the top most
     * activity
     */
    public boolean hasPackageChanged(String pName) {
        if (mLastPackageName != null) {
            return !mLastPackageName.equalsIgnoreCase(pName);
        }
        return true;
    }

    private void startTask(AndroidTask task) {
        if (task == null) {
            return;
        }
        try {
            synchronized (mOutToServer) {

                // Tells server to start next task
                mOutToServer.writeBytes(Boolean.TRUE + "\n");
                // Setup parser
                final Gson gson = new GsonBuilder().create();
                String s = gson.toJson(task);

                Log.e(TAG, s);
                mOutToServer.writeBytes(s + "\n");

                task.copyFrom(receiveTask());

                if (task.isCurrentlyRunning()) {
                    // Success start
                    Log.e(TAG, "Success");
                    mCurrentTask = task;
                    task.onStart();
                    waitForIt();
                } else {
                    // Blocked :(
                    Log.e(TAG, "blocked");
                    task.onStop();
                    // TODO: KILL CURRENT ACTIVITY?
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void stopTask(AndroidTask task) {
        if (task == null) {
            return;
        }
        try {
            synchronized (mOutToServer) {
                // Tells server to stop the task
                mOutToServer.writeBytes(Boolean.FALSE + "\n");

                // Setup parser
                final Gson gson = new GsonBuilder().create();
                String s = gson.toJson(task);

                Log.e(TAG, s);
                mOutToServer.writeBytes(s + "\n");
                
                getCurrentTask().onStop();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private AndroidTask receiveTask() throws IOException {
        synchronized (mInFromServer) {
            final Gson gson = new GsonBuilder().create();

            String json = mInFromServer.readLine().trim();

            System.out.println(json);

            AndroidTask t = gson.fromJson(json, AndroidTask.class);

            return t;
        }
    }

    private void waitForIt() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Task t = receiveTask();
                    if (!t.isCurrentlyRunning()) {
                        Log.e(TAG, "Task:  " + t.getId() + " was killed remotely");
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                // Go home
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(startMain);
                            }
                        });
                        mCurrentTask = null;
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void proposeTask(Task task) {
        if (task == null) {
            // If does not exist or already tried to launch, do nothing
            return;
        } else if (getCurrentTask() != null) {
            // stopTask(getCurrentTask());
//            mCurrentTask = null;
        }

        startTask((AndroidTask) task);
    }

    @Override
    public AndroidTask getCurrentTask() {
        return mCurrentTask;
    }

}
