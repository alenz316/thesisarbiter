package lenz.coffee.touch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class InitiatorActivity extends Activity implements OnClickListener {

    public static final long POST_DELAY = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initiator_activity);

        findViewById(R.id.start).setOnClickListener(this);
        findViewById(R.id.stop).setOnClickListener(this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                startService(getInputServiceIntent());
                finish();
            }
        }, POST_DELAY);
    }

    private Intent getInputServiceIntent() {
        Intent intent = new Intent(getApplicationContext(), InputService.class);
        intent.addCategory(InputService.TAG);
        return intent;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.start) {
            startService(getInputServiceIntent());
        } else if (v.getId() == R.id.stop) {
            stopService(getInputServiceIntent());
        }
    }

    private void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
