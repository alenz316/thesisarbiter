package lenz.coffee.touch;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lenz.coffee.ServerDriver;
import lenz.coffee.interaction.AndroidDispatcher;
import lenz.coffee.task.ReflexTaskUriConstants;
import android.app.ActivityManager;
import android.app.Instrumentation;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.Toast;

public class InputService extends Service {
    public static final String TAG = InputService.class.getSimpleName();

    private Handler mHandler;
    private int mScreenHeight;
    private int mScreenWidth;

    @Override
    public void onCreate() {
        super.onCreate();
        toast("Starting");
        Log.e(TAG, "Starting");

        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);

        mHandler = new Handler();
        mScreenHeight = metrics.heightPixels;
        mScreenWidth = metrics.widthPixels;

        Timer t = new Timer();
        t.schedule(new TimerTask() {

            @Override
            public void run() {
                new Thread(new Toucher()).start();
            }
        }, 500);

        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                new Thread(AndroidSupervisoryAttentionalSubSystem.instance(InputService.this, mHandler)).start();
            }
        }, 500, 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        toast("Stopping");
    }

    private void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Cannot bind to this service
        return null;
    }

    private static class TouchEventHolder {
        public static final int MAX_TOUCHES = 5;

        int id;
        long startTime;
        float x;
        float y;
        int action;
    }

    private class Toucher implements Runnable {

        Instrumentation mInstrument;
        TouchEventHolder[] mHolders;

        public Toucher() {
            mInstrument = new Instrumentation();

            mHolders = new TouchEventHolder[TouchEventHolder.MAX_TOUCHES];
            for (int i = 0; i < mHolders.length; i++) {
                mHolders[i] = new TouchEventHolder();
                mHolders[i].id = i;
                mHolders[i].action = MotionEvent.ACTION_CANCEL;
            }
        }

        @Override
        public void run() {
            // Template derived from
            // http://systembash.com/content/a-simple-java-tcp-server-and-tcp-client/
            try {
                Socket clientSocket = new Socket(ServerDriver.SERVER_IP, ServerDriver.SERVER_PORT);
                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream()));

                // Establish connection as touch client
                outToServer.writeBytes("t_h\n");

                while (clientSocket.isConnected()) {
                    handleMessage(inFromServer.readLine().trim());
                }
                // System.out.println("FROM SERVER: " + modifiedSentence);
                // if ("swipe".equalsIgnoreCase(modifiedSentence)) {
                // swipe();
                // }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

        private void handleMessage(String message) {
            Log.e(TAG, message);

            if (AndroidDispatcher.isGesture(message)) {
                handleGestures(message);
            } else if (AndroidDispatcher.isReflex(message)) {
                String uri = message.replaceFirst(AndroidDispatcher.REFLEX_COMMAND, "");
                boolean kill = uri.charAt(0) == '1';// kill bit/char :P
                handleReflex(uri.substring(1), kill);
            } else {

                // Should be in the format
                // "<id:int> <action:int> <x:float> <y:float>"
                // *Note: action follows MotionEvent
                String[] parts = message.split(" ");
                if (parts.length != 4) {
                    throw new IllegalArgumentException("Invalid message: " + message);
                }

                int index = Integer.parseInt(parts[0]);
                TouchEventHolder holder = mHolders[index];
                holder.action = Integer.parseInt(parts[1]);
                holder.x = mScreenWidth * Float.parseFloat(parts[2]);
                holder.y = mScreenHeight * Float.parseFloat(parts[3]);

                if (holder.action == MotionEvent.ACTION_DOWN) {
                    holder.startTime = SystemClock.uptimeMillis();
                }

                processEvent(holder);
            }
        }

        private void handleGestures(String gesture) {
            if (AndroidDispatcher.isLeftSwipe(gesture)) {
                swipeLeft();
            } else if (AndroidDispatcher.isRightSwipe(gesture)) {
                swipeRight();
            } else if (AndroidDispatcher.isRightFling(gesture)) {
                swipeUp();
            } else if (AndroidDispatcher.isLeftFling(gesture)) {
                swipeDown();
            } else {
                Log.e(TAG, gesture);
            }
        }

        private void handleReflex(String reflexUri, boolean kill) {
            if (ReflexTaskUriConstants.REFLEX_MUSIC.equals(reflexUri)) {
                return; // Do nothing
            }

            if (kill) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // Go home
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                    }
                });
            } else {
                try {
                    Log.i(TAG, reflexUri);
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(reflexUri));
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } catch (ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        }

        private void processEvent(TouchEventHolder event) {
            MotionEvent e = MotionEvent.obtain(event.startTime, SystemClock.uptimeMillis(), event.action, event.x,
                    event.y, 0);
            mInstrument.sendPointerSync(e);
            e.recycle();
        }

        private void swipeLeft() {
            swipeHorizontal(false);
        }

        private void swipeRight() {
            swipeHorizontal(true);
        }
        
        private void swipeUp() {
            swipeVertically(true);
        }
        
        private void swipeDown() {
            swipeVertically(false);
        }

        private void swipeHorizontal(boolean isRight) {
            long start = SystemClock.uptimeMillis();
            float x = isRight ? 1000 : 0;
            float xDelta = isRight ? -15 : 15;
            float y = 500;
            MotionEvent e;

            Log.e(TAG, "Toucher touching");

            Instrumentation ins = new Instrumentation();
            e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, x, y, 0);
            ins.sendPointerSync(e);
            e.recycle();
            for (int i = 0; i < 15; i++) {
                x = x + xDelta;
                e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, x, y, 0);
                ins.sendPointerSync(e);
                e.recycle();
            }
            e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, x, y, 0);
            ins.sendPointerSync(e);
            e.recycle();
        }
        
        private void swipeVertically(boolean isUp) {
            long start = SystemClock.uptimeMillis();
            float y = isUp ? 1000 : 0;
            float yDelta = isUp ? -15 : 15;
            float x = 500;
            MotionEvent e;

            Log.e(TAG, "Toucher touching");

            Instrumentation ins = new Instrumentation();
            e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, x, y, 0);
            ins.sendPointerSync(e);
            e.recycle();
            for (int i = 0; i < 15; i++) {
                y = y + yDelta;
                e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, x, y, 0);
                ins.sendPointerSync(e);
                e.recycle();
            }
            e = MotionEvent.obtain(start, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, x, y, 0);
            ins.sendPointerSync(e);
            e.recycle();
        }

    }

}
