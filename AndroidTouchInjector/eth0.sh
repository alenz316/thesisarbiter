#!/system/bin/sh
# The script enables ethernet on INFORCE-SYS6440/IFC6410 and configures gateway ip and MAC address.
#Please edit the gateway ip and MAC address and reboot the device, to make it functional.

echo "Ethernet Service \n"
#Waiting for netcfg to be up
sleep 15
#Configuring MAC i.d
echo "Configuring MAC \n"
netcfg eth0 down
netcfg eth0 hwaddr 00:50:c2:f3:9f:d6
sleep 15
netcfg eth0 up
echo "Configuring DHCP \n"
netcfg eth0 dhcp
sleep 8
netcfg eth0 up
echo "Configuring Gateway ip \n"
#configuring the Network Gateway i.p
route add default gw 129.65.26.250 eth0

